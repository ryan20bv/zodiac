<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>ZODIAC ACTIVITY</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/darkly/bootstrap.css">
</head>
<body class="bg-info ">
	<img src="">
	<div>
		<h1 class="text-center text-uppercase p-5">KNOW YOUR ZODIAC</h1>
		<div class="col-lg-4 offset-lg-4">
			<form action="controller/zodcont.php" method="POST" class="bg-light p-4 ">
				<div class="">
					<label for="name">NAME:</label>
					<input type="text" name="name" class="form-control">
					
				</div>
				<div class="">
					<label for="birthMonth">Birth Month:</label>
					<input type="number" name="birthMonth" class="form-control">
					
				</div>
				<div class="">
					<label for="birthDay">Birth Day:</label>
					<input type="number" name="birthDay" class="form-control">
					
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-primary">Check your Zodiac</button>
				</div>
				


			</form>
			
		</div>


	</div>


</body>
</html>